'use strict';

const debug = require('debug')('nightmare:actions');

module.exports = function mouseup(Nightmare) {
    if (!Nightmare) {
        Nightmare = require('nightmare');
    }

    Nightmare.action(
        'mouseup',
        function (selector, done) {
            debug('.mouseup() on ' + selector);
            this.evaluate_now(function (selector) {
                var element = document.querySelector(selector);
                if (!element) {
                    throw new Error('Unable to find element by selector: ' + selector);
                }
                var event = document.createEvent('MouseEvent');
                event.initEvent('mouseup', true, true);
                element.dispatchEvent(event);
            }, done, selector);
        });
}